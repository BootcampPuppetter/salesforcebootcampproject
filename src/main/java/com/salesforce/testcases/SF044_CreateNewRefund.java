package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SF044_CreateNewRefund extends PreAndPost {


@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Create new refund";
	authors = "Athira Vibin";
	category = "Smoke";
	nodes="Create new refund";
	}


@Test(dataProvider="fetchData")
public void createAccount()  {
	new LoginPage(driver)
	.login()
	.homeAction();
		
   }
}

