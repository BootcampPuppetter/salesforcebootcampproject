package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SF0114_EditAccount extends PreAndPost {
	
@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Edit an Account";
	authors = "Athira Vibin";
	category = "Smoke";
	dataSheetName="Edit";
	nodes="Edit Account";
	}


@Test(dataProvider="fetchData",dependsOnMethods= {"com.salesforce.testcases.SF0113_CreateAccount.createAccount"})
public void editAccount(String Name,String billAdd,String shipAdd,String phone)   {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickAccount()
	.searchAccount(Name)
	.clickEdit()
	.enterDetailsEdit(billAdd, shipAdd, phone)
	.clickSave()
	.verifyEditAccount();
		
   }
}

