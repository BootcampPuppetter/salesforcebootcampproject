package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SF0113_CreateAccount extends PreAndPost {


@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Create Account";
	authors = "Athira Vibin";
	category = "Smoke";
	dataSheetName="create";
	nodes="Create Account";
	}

@Test(dataProvider="fetchData")
public void createAccount(String Name)  {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickAccount()
	.clickNew()
	.enterName(Name)
	.dropDownSelect()
	.clickSave()
    .verifyCreateAccount(Name);
		
   }
}

