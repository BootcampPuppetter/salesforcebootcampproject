package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SFO116_CreateCase extends PreAndPost
{
	
@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Create Account";
	authors = "Athira Vibin";
	category = "Smoke";
	//dataSheetName="create";
	nodes="Create Case";
	}

@Test
public void createCase()  {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickMoreOption()
	.clickCases()
	.clickNewCase()
	.enterContactName("Ganga")
	.selectStatus()
	.enterSubjectName("Testing")
	.enterDescription("Dummy")
	.clickCaseOrigin()
	.clickNewCaseSave()
	.verifyCaseStatus("New");
   }
}

