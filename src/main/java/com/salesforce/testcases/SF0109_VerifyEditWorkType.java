package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SF0109_VerifyEditWorkType extends PreAndPost {


@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Verify Edit work type";
	authors = "Athira Vibin";
	category = "Smoke";
	nodes="Edit work type";
	}


@Test
public void verifyWorkType()  {
	new LoginPage(driver)
	.login()
	.clickToggle()
	.clickViewAll()
	.clickWorkType()
	.clickEdit()
	.enterTime()
	.clickSave()
	.verifyInvalidTimeError();
	
   }
}

