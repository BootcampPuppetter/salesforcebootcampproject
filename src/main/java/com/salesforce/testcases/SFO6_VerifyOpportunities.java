package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SFO6_VerifyOpportunities extends PreAndPost {


@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Verify Opportunity";
	authors = "Athira Vibin";
	category = "Smoke";
	nodes="Verify opportunity";
	}


@Test
public void verifyoppotunity()  {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickOpportunity()
	.clickTable()
	.verifyCloseDateOrder();
	
   }
}

