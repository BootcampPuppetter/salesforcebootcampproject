package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SFO133_CreateContact extends PreAndPost{

@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Create Contact";
	authors = "Sakthi Parthiban";
	category = "Smoke";
	dataSheetName="createcontact";
	nodes="Create Contact";
	}


@Test(dataProvider="fetchData")
public void createContact(String name) throws InterruptedException  {
	new LoginPage(driver)
	.login()
	.clickPlusIcon()
	.clickNewContact()
	.selectMrOptionInSalutation()
	.EnterFirstAndLastName()
	.EnterEmailAddress()
	.FillAccountDetails()
	.ClickSaveContactDetails()
	.verifyCreatedContact();
   }
}



