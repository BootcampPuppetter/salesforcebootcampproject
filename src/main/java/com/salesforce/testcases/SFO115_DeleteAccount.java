package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SFO115_DeleteAccount extends PreAndPost {
	
@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Delete an Account";
	authors = "Athira Vibin";
	category = "Smoke";
	dataSheetName="create";
	nodes="Delete Account";
	}


@Test(dataProvider="fetchData",dependsOnMethods= {"com.salesforce.testcases.SF0113_CreateAccount.createAccount","com.salesforce.testcases.SF0114_EditAccount.editAccount"})
public void editAccount(String Name)  {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickAccount()
	.searchAccount(Name)
	.clickDelete()
	.verifyDeleteAccount(Name);
   }
}

