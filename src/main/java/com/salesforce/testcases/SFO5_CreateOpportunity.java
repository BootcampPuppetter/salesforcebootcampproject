package com.salesforce.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.salesforce.pages.LoginPage;

import lib.selenium.PreAndPost;

public class SFO5_CreateOpportunity extends PreAndPost {


@BeforeTest
public void setReportDetails() {
	testCaseName = this.getClass().getSimpleName();
	testDescription = "Create Opportunity";
	authors = "Athira Vibin";
	category = "Smoke";
	nodes="Create Opportunity";
	}


@Test
public void createoppotunity()  {
	new LoginPage(driver)
	.login()
	.homeAction()
	.clickOpportunity()
	.clickNew()
	.clickDateDropDown()
	.clickSave()
	.verifyBlankError();
		
   }
}

