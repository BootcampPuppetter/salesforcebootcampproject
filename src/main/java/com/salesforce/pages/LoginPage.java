package com.salesforce.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LoginPage extends PreAndPost {
	public LoginPage(EventFiringWebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);

	}
	
	public HomePage login() {
		enterUserName("nupela@testleaf.com");
		enterPassword("Bootcamp$123");
		clickLogin();
		reportStep("The login is successful", "pass");
		return new HomePage(driver);
	}

	@FindBy(how = How.ID, using = "username")
	private WebElement user_name;

	public LoginPage enterUserName(String str) {
		type(user_name, str);
		reportStep("The UserName:" + str + "entered successfully at login page", "pass");
		return this;
	}

	// action enter
	@FindBy(how = How.ID, using = "password")
	private WebElement passWd;

	public LoginPage enterPassword(String password) {

		type(passWd, password);
		reportStep("The Password:" + password + "entered successfully at login page", "pass");
		return this;
	}

	// action click
	@FindBy(how = How.ID, using = "Login")
	private WebElement login;

	public HomePage clickLogin() {
		click(login);
		reportStep("Login button is clicked successfully at login page", "pass");
		return new HomePage(driver);
	}

}
