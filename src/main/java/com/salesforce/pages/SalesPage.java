package com.salesforce.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SalesPage extends PreAndPost {
	public SalesPage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// click Accounts
	@FindBy(how = How.XPATH, using = "//span[text()='Accounts']")
	private WebElement clickAccount;

	public AccountPage clickAccount() {
		clickJavascript(clickAccount);
		reportStep("Account is clicked successfully in sales page", "pass");
		return new AccountPage(driver);
	}
	
	//click opportunity
	
	@FindBy(how = How.XPATH, using = "//span[text()='Opportunities']")
	private WebElement clickOpportunity;

	public OpportunityPage clickOpportunity() {
		clickJavascript(clickOpportunity);
		reportStep("Opportunities is clicked successfully in sales page", "pass");
		return new OpportunityPage(driver);
	}
	
	//click More
	@FindBy(how = How.XPATH, using = "//span[text()='More']")
	private WebElement clkMore;

	public SalesPage clickMoreOption() {
		clickJavascript(clkMore);
		reportStep("More dropdown is clicked successfully in sales page", "pass");
		return this;
	}
	
	//Click Cases
	@FindBy(how =How.XPATH,using = "//span[text()='Cases']" )
	private WebElement clkCases;

	public CasesPage clickCases() {
		clickJavascript(clkCases);
		reportStep("Cases is clicked successfully in sales page", "pass");
		return new CasesPage(driver);
	}
	
	//Click Contact
		@FindBy(how =How.XPATH,using = "//span[text()='Contacts']" )
		private WebElement contacts;

		public ContactPage clickContacts() {
			clickJavascript(contacts);
			reportStep("Contacts is clicked successfully in sales page", "pass");
			return new ContactPage(driver);
		}

	
}
