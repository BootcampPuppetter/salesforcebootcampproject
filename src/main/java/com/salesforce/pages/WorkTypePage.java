package com.salesforce.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class WorkTypePage extends PreAndPost {
	public WorkTypePage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Click Edit drop down
	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-icon_container'])[2]")
	private WebElement clickDropDown;
	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	private WebElement clickEdit;

	public WorkTypePage clickEdit() {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(clickDropDown));
		click(clickDropDown);
		wait.until(ExpectedConditions.visibilityOf(clickEdit));
		click(clickEdit);
		reportStep("Edit drop down is clicked", "pass");
		return this;
	}
	// Click Edit drop down
		@FindBy(how = How.XPATH, using = "(//span[text()='Timeframe Start']/following::input)[1]")
		private WebElement typeTimeFrameStart;
		@FindBy(how = How.XPATH, using = "(//span[text()='Timeframe Start']/following::input)[2]")
		private WebElement typeTimeFrameEnd;

		public WorkTypePage enterTime() {
			WebDriverWait wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.visibilityOf(typeTimeFrameStart));
		    type(typeTimeFrameStart,"9");
			wait.until(ExpectedConditions.visibilityOf(typeTimeFrameEnd));
			type(typeTimeFrameEnd,"6");
			reportStep("Time frame is entered successfully", "pass");
			return this;
		}
		
		//click save
		@FindBy(how = How.XPATH, using = "//button[@title='Save']/span")
		private WebElement saveButton;
	    public VerificationPage clickSave() {
			click(saveButton);
			reportStep("Save button is clicked successfully", "pass");
			return new VerificationPage(driver);

		}
}
