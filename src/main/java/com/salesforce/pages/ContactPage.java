package com.salesforce.pages;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import lib.selenium.PreAndPost;

public class ContactPage extends PreAndPost{
	public ContactPage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
    //filling values in create contact
    //salutation
    @FindBy(how = How.XPATH, using = "(//a[contains(text(),'--None--')])[1]")
	private WebElement Salution;
    @FindBy(how = How.XPATH, using = "//*[contains(@title, 'Mr.')]")
   	private WebElement MrOption;
	public ContactPage selectMrOptionInSalutation() {
		click(Salution);
		click(MrOption);
		reportStep("Mr option selected successfully from Salution", "pass");
		return this;
	}
   
	//FirstnameAndLastname
    @FindBy(how = How.XPATH, using = "//*[contains(@class,'firstName')]")
	private WebElement firstname;
    @FindBy(how = How.XPATH, using = "//*[contains(@class,'lastName')]")
	private WebElement lastname;
    //adding random number at end of contact to make it unique
    Random randomGenerator = new Random();
    int randomInt = randomGenerator. nextInt(1000);
    public ContactPage EnterFirstAndLastName() {
    	firstname.sendKeys("Sakthi");//firstname
    	reportStep("Firstname Entered Successfully", "pass");
    	lastname.sendKeys("Parthiban"+randomInt);//lastname+randomInt
		reportStep("Lastname Enetered successfully", "pass");
		return this;
	}
    
    //email
    @FindBy(how = How.XPATH, using = "//input[@inputmode='email']")
   	private WebElement email;
    public ContactPage EnterEmailAddress() {
    	email.sendKeys("sakthi@test.com");//emailaddress
		reportStep("Email address entered successfully", "pass");
		return this;
	}
    
    //fillAccountDetails
    @FindBy(how = How.XPATH, using = "//*[contains(@title, 'Search Accounts')]")
   	private WebElement searchaccountstextox;
    @FindBy(how = How.XPATH, using = "//span[contains(@title,'New Account')]")
   	private WebElement newaccount;
    @FindBy(how = How.XPATH, using = "(//input[@class=' input'])[4]")
   	private WebElement accountname;
    @FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
   	private WebElement saveaccountdetails;
      
    public ContactPage FillAccountDetails() throws InterruptedException {
    	click(searchaccountstextox);
    	click(newaccount);
    	accountname.sendKeys("Credits"+randomInt);//accountname
    	click(saveaccountdetails);
		reportStep("Account Details entered successfully", "pass");
		Thread.sleep(2000);
		return this;
	}
    

    //Click overall save in contact details page
    @FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
   	private WebElement savecontactdetails;
	public VerificationPage ClickSaveContactDetails() {
		click(savecontactdetails);
		reportStep("Click Save Contact details", "pass");
		return new VerificationPage(driver);
	}
    
}









