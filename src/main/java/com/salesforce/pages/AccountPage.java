package com.salesforce.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AccountPage extends PreAndPost {
	public AccountPage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// click new
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	private WebElement clickNew;

	public AccountPage clickNew() {
		click(clickNew);
		reportStep("New botton is clicked", "pass");
		return this;

	}

	// enter value in text field
	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	private WebElement username;

	public AccountPage enterName(String name) {
		type(username, name);
		reportStep("The UserName:" + name + "entered successfully at login page", "pass");
		return this;

	}

	// Click Edit drop down
	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-icon_container'])[2]")
	private WebElement clickDropDown;
	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	private WebElement clickEdit;

	public AccountPage clickEdit() {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(clickDropDown));
		click(clickDropDown);
		wait.until(ExpectedConditions.visibilityOf(clickEdit));
		click(clickEdit);
		reportStep("Edit drop down is clicked", "pass");
		return this;
	}

//Click drop down and select Public
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[3]")
	private WebElement selectPublic;
	@FindBy(how = How.XPATH, using = "//a[@title='Public']")
	private WebElement optionPublic;

	public AccountPage dropDownSelect() {
		click(selectPublic);
		click(optionPublic);
		reportStep("Public is selected from dropdown", "pass");
		return this;
	}

//Search account and click enter
	@FindBy(how = How.XPATH, using = "//input[@name='Account-search-input']")
	private WebElement searchAccount;
    public AccountPage searchAccount(String name) {
		type(searchAccount, name);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(searchAccount));
		searchAccount.sendKeys(Keys.ENTER);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		reportStep("Account is searched with: " + name + " and Enter key is pressed", "pass");
		return this;

	}

	// Click drop down and delete the account

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'slds-table forceRecordLayout')]/tbody[1]/tr[1]/td[6]/span[1]/div[1]/a[1]/lightning-icon[1]/lightning-primitive-icon[1]")
	private WebElement clickdrop;
	@FindBy(how = How.XPATH, using = "//a[@title='Delete']")
	private WebElement clickDelete;
	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	private WebElement confirmDelete;

	public VerificationPage clickDelete()  {
		click(clickdrop);
		click(clickDelete);
		click(confirmDelete);
		reportStep("Account is deleted", "pass");
		return new VerificationPage(driver);
	}
	public AccountPage enterDetailsEdit(String billAdd,String shipAdd,String phone) {
		selectTechnologyPartner();
		selectHealthcare();
		enterBillingStreet(billAdd);
		enterShippingStreet(shipAdd);
		selectPriorityLow();
		selectSLASliver();
		selectActive();
        enterPhone(phone);
		selectOpportunity();
        reportStep("Create account is edited successful", "pass");
		return this;
	}
	// Select Type as Technology Partner
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	private WebElement selectTechnologyPartner;
	@FindBy(how = How.XPATH, using = "//a[@title='Technology Partner']")
	private WebElement optionTechnologyPartner;

	public AccountPage selectTechnologyPartner() {
		click(selectTechnologyPartner);
		click(optionTechnologyPartner);
		reportStep("Type is selected as Technology Partner", "pass");
		return this;
	}

// Select Industry as Healthcare
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[4]")
	private WebElement selectHealthcare;
	@FindBy(how = How.XPATH, using = "//a[@title='Healthcare']")
	private WebElement optionHealthcare;

	public AccountPage selectHealthcare() {
		click(selectHealthcare);
		click(selectHealthcare);
		reportStep("Industry is selected as Healthcare", "pass");
		return this;
	}

	// Enter Billing Address
	@FindBy(how = How.XPATH, using = "//textarea[@placeholder='Billing Street']")
	private WebElement enterBillAdd;
    public AccountPage enterBillingStreet(String billAdd) {
		type(enterBillAdd, billAdd);
		reportStep("Billing address entered", "pass");
		return this;
	}

// Enter shipping Address
	@FindBy(how = How.XPATH, using = "//textarea[@placeholder='Shipping Street']")
	private WebElement enterShipAdd;

	public AccountPage enterShippingStreet(String shipAdd) {
		type(enterShipAdd, shipAdd);
		reportStep("Shipping address entered", "pass");
		return this;
	}

//Select Customer Priority as Low
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[5]")
	private WebElement selectPriority;
	@FindBy(how = How.XPATH, using = "//a[@title='Low']")
	private WebElement optionLow;

	public AccountPage selectPriorityLow() {
		click(selectPriority);
		click(optionLow);
		reportStep("Customer Priority is selected as Low", "pass");
		return this;
	}

//Select SLA as Silver
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[5]")
	private WebElement selectSLA;
	@FindBy(how = How.XPATH, using = "//a[@title='Low']")
	private WebElement optionSilver;

	public AccountPage selectSLASliver() {
		click(selectSLA);
		click(optionSilver);
		reportStep("Select SLA as Silver", "pass");
		return this;
	}

// Select Active as NO
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[8]")
	private WebElement selectActive;
	@FindBy(how = How.XPATH, using = "//a[@title='No']")
	private WebElement optionNO;

	public AccountPage selectActive() {
		click(selectActive);
		click(optionNO);
		reportStep("Select Active as No", "pass");
		return this;
	}

//Enter Unique Number in Phone Field
	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[2]")
	private WebElement enterPhone;

	public AccountPage enterPhone(String phone) {
		type(enterPhone, phone);
		reportStep("Phone number is entered as: "+phone+" successfully", "pass");
		return this;
	}

//Select Upsell Opportunity as No
	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[7]")
	private WebElement selectOpportunity;
	@FindBy(how = How.XPATH, using = "(//a[@title='No'])[2]")
	private WebElement optionNo;

	public AccountPage selectOpportunity() {
		click(selectOpportunity);
		click(optionNo);
		reportStep("Select Upsell Opportunity as No", "pass");
		return this;
	}

	//click save
		@FindBy(how = How.XPATH, using = "//button[@title='Save']/span")
		private WebElement saveButton;
	    public VerificationPage clickSave() {
			click(saveButton);
			reportStep("Save button is clicked successfully", "pass");
			return new VerificationPage(driver);

		}
}