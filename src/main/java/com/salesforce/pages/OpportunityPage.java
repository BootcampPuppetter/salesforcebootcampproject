package com.salesforce.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class OpportunityPage extends PreAndPost {
	public OpportunityPage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	// Click new button
	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	private WebElement clickNew;

	public OpportunityPage clickNew() {
		click(clickNew);
		reportStep("New botton is clicked", "pass");
		return this;
	}

	// Choose Close date as Tomorrow Date
	Date date = new Date();
	DateFormat sample = new SimpleDateFormat("dd");
	String today = sample.format(date);
	int tomorrow = Integer.parseInt(today) + 1;
	String tommorrowDate = Integer.toString(tomorrow);

	@FindBy(how = How.XPATH, using = "//a[@class='datePicker-openIcon display']")
	private WebElement clickDateDropDown;
	@FindBy(how = How.XPATH, using = "//table[@class='calGrid']/tbody[1]/tr/td/span[text()='18']")
	private WebElement selectDateTommorrow;

	public OpportunityPage clickDateDropDown() {
		click(clickDateDropDown);
		reportStep("Date drop down is clicked", "pass");
		click(selectDateTommorrow);
		reportStep("Date is selected", "pass");
		return this;
	}

	// click save
	@FindBy(how = How.XPATH, using = "//button[@title='Save']/span")
	private WebElement saveButton;

	public VerificationPage clickSave() {
		click(saveButton);
		reportStep("Save button is clicked successfully", "pass");
		return new VerificationPage(driver);

	}

	// Click table
    @FindBy(how = How.XPATH, using = "//table[contains(@class,'slds-table forceRecordLayout')]/thead[1]/tr[1]/th[7]/div[1]/a[1]")
   	private WebElement sortTable;
    
	public VerificationPage clickTable() {
		click(sortTable);
		return new VerificationPage(driver);
	}
	 
}
