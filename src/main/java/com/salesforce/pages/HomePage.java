package com.salesforce.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class HomePage extends PreAndPost {
	public HomePage(EventFiringWebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);

	}
	//Home Page actions(Click toggle,click view all,Click sales)
	public SalesPage homeAction() {
		clickToggle();
		clickViewAll();
		clickSales();
		return new SalesPage(driver);
	}

	//click Toggle 
	@FindBy(how=How.CLASS_NAME,using="slds-icon-waffle")
	private WebElement clkToggle;
	public HomePage clickToggle() {
		click(clkToggle);
		reportStep("Toggle is clicked successful", "pass");
		return this;
	}

	// click view all
	@FindBy(how=How.XPATH,using="//button[@class='slds-button']")
	private WebElement clkViewAll;
	public HomePage clickViewAll() {
		click(clkViewAll);
		reportStep("View all is clicked successful", "pass");
		return this;
	}

	// click sales
	@FindBy(how=How.XPATH,using="//p[text()='Sales']") 
	private WebElement clkSales;
	public SalesPage clickSales() {
		click(clkSales);
		reportStep("Sales is clicked successful", "pass");
		return new SalesPage(driver);
	}

	//click Work type
	@FindBy(how=How.XPATH,using="//input[@placeholder='Search apps or items...']") 
	private WebElement searchWorkType;
	@FindBy(how=How.XPATH,using=" (//p[@class='slds-truncate']/mark[text()='Work Type'])[2]") 
	private WebElement clickWorkType;
	public WorkTypePage clickWorkType() {
		type(searchWorkType, "Work Type");
		click(clickWorkType);
		reportStep("Work Type is clicked successful", "pass");
		return new WorkTypePage(driver);
	}

	//Click plus icon in home page
	@FindBy(how = How.XPATH, using = "//*[@class='slds-icon slds-icon_x-small']")
	private WebElement PlusIcon;

	public  HomePage clickPlusIcon() throws InterruptedException {
		Thread.sleep(10000);
		click(PlusIcon);
		reportStep("Plus icon is clicked successfully in home page", "pass");
		return this;
	}
	
	//Click on New contact
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'New Contact')]")
	private WebElement NewContact;
	public ContactPage clickNewContact() {
		clickJavascript(NewContact);
		reportStep("New Contact is clicked successfully from plus button options", "pass");
		return new ContactPage(driver);
	}
}
