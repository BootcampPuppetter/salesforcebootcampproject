package com.salesforce.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class VerificationPage extends PreAndPost {
	public VerificationPage(EventFiringWebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(how = How.XPATH, using = "//input[@name='Account-search-input']")
	private WebElement clicksearch;
	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	private WebElement noItemDisplayed;

	public VerificationPage verifyDeleteAccount(String name) {
		type(clicksearch, name);
		clicksearch.sendKeys(Keys.ENTER);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(noItemDisplayed));
		String displayedValue = noItemDisplayed.getText();
		if (displayedValue.equals("No items to display.")) {
			verifyDisplayed(noItemDisplayed);
			reportStep("Account is Deleted successfully", "pass");
		}
		return this;
	}

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	private WebElement displayedItem;

	public VerificationPage verifyCreateAccount(String name) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(displayedItem));
		String displayedValue = displayedItem.getText();
		if (displayedValue.contains(name)) {
			verifyDisplayed(displayedItem);
			reportStep("Account is created successfully", "pass");
		}
		return this;
	}

	@FindBy(how = How.XPATH, using = "//span[text()='(812) 978-5540']")
	private WebElement displayedEdit;

	public VerificationPage verifyEditAccount() {
		String displayedValue = displayedEdit.getText();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(displayedEdit));
		if (displayedValue.equals("(812) 978-5540")) {
			verifyDisplayed(displayedItem);
			reportStep("Account is Edited successfully", "pass");
		}
		return this;
	}

	@FindBy(how = How.XPATH, using = "//ul[@class='errorsList']/li")
	private WebElement textErrorMessage;

	public VerificationPage verifyBlankError() {
		String displayedValue = textErrorMessage.getText();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(textErrorMessage));
		if (displayedValue.equals("These required fields must be completed: Opportunity Name, Stage")) {
			verifyDisplayed(textErrorMessage);
			reportStep("Error message is listed successfully", "pass");
		}
		return this;
	}

	public VerificationPage verifyCloseDateOrder() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<WebElement> list = driver.findElements(By.xpath(
				"//table[contains(@class,'slds-table forceRecordLayout')]/tbody[1]/tr/td[6]/span[1]/span[1]/span[1]"));
		int size = list.size();
		System.out.println(size);
		List<String> actual = new ArrayList<String>();
		List<String> sorted = new ArrayList<String>();
		String s1 = null;
		String s2 = null;
		for (int i = 1; i < size; i++) {
			actual.add(list.get(i).getText().replaceAll("\\D", " ").substring(0, 2));
			sorted.add(list.get(i).getText().replaceAll("\\D", " ").substring(0, 2));
			Collections.sort(sorted, Collections.reverseOrder());
			s1 = actual.toString();
			s2 = sorted.toString();
		}
		System.out.println(s1);
		System.out.println(s2);
		if (s1.equals(s2)) {
			reportStep("Close date is sorted", "pass");
		}
		return this;
	}

	@FindBy(how = How.XPATH, using = "(//li[@class='form-element__help'])[1]")
	private WebElement textInvalidTimeErrorMessageStart;
	@FindBy(how = How.XPATH, using = "(//li[@class='form-element__help'])[2]")
	private WebElement textInvalidTimeErrorMessageEnd;
	public VerificationPage verifyInvalidTimeError() {
		String displayedValue1 = textInvalidTimeErrorMessageStart.getText();
		String displayedValue2 = textInvalidTimeErrorMessageEnd.getText();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(textInvalidTimeErrorMessageStart));
		if (displayedValue1.equals(
				"Enter a Timeframe End number that is greater than the Timeframe Start number.: Timeframe Start")) {
			verifyDisplayed(textInvalidTimeErrorMessageStart);
			reportStep("Error message is listed successfully for start date", "pass");
		}
		if (displayedValue2.equals(
				"Enter a Timeframe End number that is greater than the Timeframe Start number.: Timeframe End")) {
			verifyDisplayed(textInvalidTimeErrorMessageEnd);
			reportStep("Error message is listed successfully for end date", "pass");
		}
		return this;
	}
	
    
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Contact')])[2]")
	private WebElement contactsuccessmessage;
	public VerificationPage verifyCreatedContact() {
		boolean verifyContactCreated= contactsuccessmessage.isDisplayed();
	    if(verifyContactCreated) {
	    	reportStep(""+contactsuccessmessage.getText(), "pass");
	    }
	    else {
	    	reportStep("Contact not created successfully", "fail");
	    }
		return this;
	}
	

	//Verify the new case saved details
	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New'][1]")
	private WebElement chkcaseStatus;
	@FindBy(how = How.XPATH, using = "//p[text()='Case Number']/following::lightning-formatted-text")
	private WebElement chkcasenumber;		
	public VerificationPage verifyCaseStatus(String status1)
	{
		String checkStatus = chkcaseStatus.getText();
		if (checkStatus.contains(status1))
		{
			String newcasenum = chkcasenumber.getText();
			reportStep("NewCase" + newcasenum +" is created successfully", "pass");
			return this;
		}
		else
		{
			System.out.println("NewCase not Created Successfully");
		}
	return this;
	}
}



