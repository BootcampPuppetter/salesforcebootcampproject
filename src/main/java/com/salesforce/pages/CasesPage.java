package com.salesforce.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import lib.selenium.PreAndPost;

public class CasesPage extends PreAndPost {
		public CasesPage(EventFiringWebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);

		}

		// click NewCase button
		@FindBy(how = How.LINK_TEXT,using = "New")
		private WebElement clkNew;
		public CasesPage clickNewCase() {
			click(clkNew);
			reportStep("NewCase botton is clicked", "pass");
			return this;
		}

		
		// Choose Contact Name as 'Your Name'
		@FindBy(how = How.XPATH, using = "//div/input[@title='Search Contacts']")
		private WebElement clkContact;
		public CasesPage enterContactName(String contactname) {
		type(clkContact, contactname);
		reportStep("Contact Name:" + contactname + "choosed successfully", "pass");
		return this;
		}
		
		
		// Select status as Escalated
		@FindBy(how = How.XPATH, using = "//label[text()='Status']/following::input")
		private WebElement clkStatus;
		@FindBy(how = How.XPATH, using = "//span[text()='Escalated']")
		private WebElement statusSelect;
		public CasesPage selectStatus() {
		click(clkStatus);
		click(statusSelect);
		reportStep("Status selected", "pass");
		return this;
		}
				
		//Enter Subject as 'Testing'
		@FindBy(how = How.XPATH, using = "//span[text()='Subject']/following::input[@class=' input'][6]")
		private WebElement clkSubject;
		public CasesPage enterSubjectName(String subjectname) {
		type(clkSubject, subjectname);
		reportStep("Subject Name:" + subjectname + "choosed successfully", "pass");
		return this;
		}
		
		//Enter Description as 'Dummy'
		@FindBy(how = How.XPATH, using = "//span[text()='Description']/following::textarea[1]")
		private WebElement clkCaseDesc;
		//Select Case Origin field value to avoid mandatory field error
		@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']/following::a[1]")
		private WebElement clkCaseOrigin;
		@FindBy(how = How.XPATH, using = "//a[@title='Phone']")
		private WebElement selctOriginValue;
		public CasesPage enterDescription(String casedesc) {
		type(clkCaseDesc, casedesc);
		reportStep("Subject Name:" + casedesc + "choosed successfully", "pass");
		return this;
		}
		public CasesPage clickCaseOrigin() {
		click(clkCaseOrigin);
		click(selctOriginValue);
		reportStep("Case Origin selected", "pass");
		return this;
		}
		
		// Click 'Save'
		@FindBy(how = How.XPATH, using = "//button[@title='Save']/span")
		private WebElement clkNewCaseSave;
		public VerificationPage clickNewCaseSave() {
		click(clkNewCaseSave);
		reportStep("Save button clicked on Create New Case page", "pass");
		return new VerificationPage(driver);
		}
		
	// click EditCase button
	@FindBy(how = How.XPATH, using = "//a[@class='rowActionsPlaceHolder slds-button slds-button--icon-x-small slds-button--icon-border-filled keyboardMode--trigger']")
	private WebElement selectCase;
	@FindBy(how = How.XPATH, using = "//div/ul/li/a[@title='Edit']")
	private WebElement clkEdit;

	public CasesPage clickEditCase() {
		click(selectCase);
		click(clkEdit);
		reportStep("EditCase button is clicked", "pass");
		return this;
	}
				
	// Select status as Working
	@FindBy(how = How.XPATH, using = "(//input[@role='textbox'])[2]")
	private WebElement editStatus;
	@FindBy(how = How.XPATH, using = "//lightning-base-combobox-item[@data-value='Working']")
	private WebElement updtStatus;
	public CasesPage updateStatus() {
		click(editStatus);
		click(updtStatus);
		reportStep("update value Status selected as Working", "pass");
		return this;
	}
			    
	//Update Priority to low
	@FindBy(how = How.XPATH, using = "//span[text()='Priority']/following::a[@class='select'][1]")
	private WebElement editPriority;
	@FindBy(how = How.XPATH, using = "//a[@title='Low']")
	private WebElement updtPriority;
	// Thread.sleep(2000);
	public CasesPage updatePriority() {
		click(editPriority);
		click(updtPriority);
		reportStep("update value Priority selected as Low", "pass");
		return this;
	}
	
	//Step8:Update SLA violation to No
	@FindBy(how = How.XPATH, using = "//span[text()='SLA Violation']/following::div[2]")
	private WebElement editSLA;
	@FindBy(how = How.XPATH, using = "//a[@title='No']")
	private WebElement editTitle;
	public CasesPage updateSLAViolationNo() {
		click(editSLA);
		click(editTitle);
		reportStep("update value SLA Violation selected", "pass");
		return this;
	}
    //Step9: Update Case Origin as Phone
	@FindBy(how = How.XPATH, using = "//span[text()='Case Origin']/following::a[1]")
	private WebElement editCaseOrigin;
	@FindBy(how = How.XPATH, using = "//a[@title='Phone']")
	private WebElement selectCaseOrigin;
	
	public CasesPage updateCaseOrigin() {
		click(editCaseOrigin);
		click(selectCaseOrigin);
		reportStep("update value Case Origin selected as Phone", "pass");
		return this;
	}
    //Step10. Click on Save and Verify Status as Working
	
		
}

